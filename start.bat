@echo off

REM Check if the virtual environment directory exists
if exist venv (
    echo Virtual environment already exists.
    echo Activating virtual environment...
    call venv\Scripts\activate

    echo Running fish.py...
    python fish.py

    REM Deactivate virtual environment after running the script
    deactivate
) else (
    echo Creating virtual environment...
    python -m venv venv

    echo Activating virtual environment...
    call venv\Scripts\activate

    echo Installing required packages...
    pip install -r requirements.txt

    echo Running fish.py...
    python fish.py

    REM Deactivate virtual environment after installation and running the script
    deactivate
)

echo Setup complete.
pause
