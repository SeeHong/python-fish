import cv2
import numpy as np
import pygame
import pyautogui
import pytesseract
import time
import re

# Constants and Configuration
MUSIC_FILE = r"./resources/assets/notification.wav"
TESSERACT_CMD = r'./resources/Tesseract-OCR/tesseract.exe'
SCREEN_REGION = (1382, 40, 1767 - 1382, 270 - 40)
KEYWORDS = {"財", "神", "紅", "包"}
WINDOW_NAME = 'Keyword Detection'

# Initialize pygame and load music
pygame.init()
pygame.mixer.music.load(MUSIC_FILE)

# Path to Tesseract executable
pytesseract.pytesseract.tesseract_cmd = TESSERACT_CMD

# Create window to display results
cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
cv2.moveWindow(WINDOW_NAME, 100, 100)

# Function to capture screen region
def capture_screen(region):
    screenshot = pyautogui.screenshot(region=region)
    return cv2.cvtColor(np.array(screenshot), cv2.COLOR_RGB2BGR)

# Function to extract text using Tesseract OCR
def extract_text(image):
    text = pytesseract.image_to_string(image, lang='chi_tra')
    return "".join(char for char in text if not char.isspace())

# Main loop
def main():
    user_id = None
    last_notification_user_id = None

    while True:
        # Capture screen region
        screen_shot = capture_screen(SCREEN_REGION)

        # Extract text from the captured image
        extracted_text = extract_text(screen_shot)

        # Detect user IDs
        is_user_id_1 = re.search(r'#\d{4}', extracted_text)
        is_user_id_2 = re.search(r'\*\d{4}', extracted_text)
        
        if is_user_id_1:
            user_id = is_user_id_1.group()
        elif is_user_id_2:
            user_id = is_user_id_2.group().replace('*', "#")

        # Check if any keyword is in the extracted text
        if any(keyword in extracted_text for keyword in KEYWORDS):
            print(f'Found keyword and comparing {last_notification_user_id} vs {user_id}')
            if user_id != last_notification_user_id:
                print(f"User ID: {user_id} matched the keyword")
                pygame.mixer.music.play()
                last_notification_user_id = user_id
                time.sleep(3)  # Wait for 3 seconds to allow the music to play

        if extracted_text:
            print(extracted_text)

        # Display the screen capture
        cv2.imshow(WINDOW_NAME, screen_shot)

        # Delay
        time.sleep(0.5)

        # Exit loop if 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release resources
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
